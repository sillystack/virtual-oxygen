import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sillytrack/helpers/ad_helper.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

import '../models/setting.dart';

ValueNotifier<Setting> setting = new ValueNotifier(new Setting());
final navigatorKey = GlobalKey<NavigatorState>();
BannerAd resiBannerAd;
bool isResiBannerAdReady = false;
BannerAd billBannerAd;
bool isBillBannerAdReady = false;

Future<Setting> initSettings() async {
  Setting _setting;
  SharedPreferences prefs = await SharedPreferences.getInstance();

  _setting = Setting.fromJSON({'app_name': 'Sillyoxygen'});
  _setting.brightness.value =
      prefs.getBool('isDark') ?? false ? Brightness.dark : Brightness.light;
  setting.value = _setting;
  // ignore: invalid_use_of_visible_for_testing_member, invalid_use_of_protected_member
  setting.notifyListeners();
  return setting.value;
}

void setBrightness(Brightness brightness) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (brightness == Brightness.dark) {
    prefs.setBool("isDark", true);
    brightness = Brightness.dark;
  } else {
    prefs.setBool("isDark", false);
    brightness = Brightness.light;
  }
}

Future<void> getResiAds() async {
  resiBannerAd = BannerAd(
    adUnitId: AdHelper.bannerAdUnitId,
    request: AdRequest(),
    size: AdSize.banner,
    listener: BannerAdListener(
      onAdLoaded: (_) {
        print('Banner Ready');
        isResiBannerAdReady = true;
      },
      onAdFailedToLoad: (ad, err) {
        print('Failed to load a banner ad: ${err.message}');
        isResiBannerAdReady = false;
        ad.dispose();
      },
    ),
  );

  resiBannerAd.load();
}

Future<void> getBillAds() async {
  billBannerAd = BannerAd(
    adUnitId: AdHelper.bannerAdUnitAltId,
    request: AdRequest(),
    size: AdSize.banner,
    listener: BannerAdListener(
      onAdLoaded: (_) {
        print('Banner Ready');
        isBillBannerAdReady = true;
      },
      onAdFailedToLoad: (ad, err) {
        print('Failed to load a banner ad: ${err.message}');
        isBillBannerAdReady = false;
        ad.dispose();
      },
    ),
  );

  billBannerAd.load();
}
