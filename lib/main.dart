import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:sillytrack/models/setting.dart';
import 'package:sillytrack/route_generator.dart';
import './repository/settings_repository.dart' as repo;

Future<void> _messageHandler(RemoteMessage message) async {
  print('background message ${message.notification?.body}');
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  MobileAds.instance.initialize();

  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_messageHandler);
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
//  /// Supply 'the Controller' for this application.
//  MyApp({Key key}) : super(con: Controller(), key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  FirebaseMessaging messaging;

  @override
  void initState() {
    repo.initSettings();
    repo.getResiAds();
    repo.getBillAds();
    super.initState();

    messaging = FirebaseMessaging.instance;
    messaging.getToken().then((value) {
      print(value);
    });
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: repo.setting,
        builder: (context, Setting _setting, _) {
          return MaterialApp(
              navigatorKey: repo.navigatorKey,
              title: _setting.appName,
              initialRoute: '/Splash',
              onGenerateRoute: RouteGenerator.generateRoute,
              debugShowCheckedModeBanner: false,
              theme: _setting.brightness.value == Brightness.light
                  ? ThemeData(
                      fontFamily: 'ProductSans',
                      primaryColor: Colors.white,
                      floatingActionButtonTheme: FloatingActionButtonThemeData(
                          elevation: 0, foregroundColor: Colors.white),
                      brightness: Brightness.light,
                      accentColor: Color(0xFF40439B),
                      // dividerColor: Colors.indigo[50],
                      focusColor: Color(0xFF40439B),
                      hintColor: Color(0xFF40439B),
                      highlightColor: Colors.black87,
                      textTheme: TextTheme(
                        headline5: TextStyle(
                            fontSize: 20.0, color: Colors.white, height: 1.3),
                        headline4: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.black87,
                            height: 1.3),
                        headline3: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.w300,
                            color: Colors.black87,
                            height: 1.3),
                        headline2: TextStyle(
                            fontSize: 22.0,
                            fontWeight: FontWeight.w300,
                            color: Colors.black87,
                            height: 1.4),
                        headline1: TextStyle(
                            fontSize: 24.0,
                            fontWeight: FontWeight.w300,
                            color: Colors.black87,
                            height: 1.4),
                        subtitle1: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.black87,
                            height: 1.3),
                        headline6: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w300,
                            color: Colors.black87,
                            height: 1.3),
                        bodyText2: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w400,
                            color: Colors.black87,
                            height: 1.2),
                        bodyText1: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.black87,
                            height: 1.3),
                        caption: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w300,
                            color: Colors.black87,
                            height: 1.2),
                      ),
                    )
                  : ThemeData(
                      fontFamily: 'ProductSans',
                      primaryColor: Color(0xFF252525),
                      brightness: Brightness.dark,
                      scaffoldBackgroundColor: Color(0xFF2C2C2C),
                      accentColor: Colors.black,
                      // dividerColor: config.Colors().accentColor(0.1),
                      focusColor: Colors.white,
                      hintColor: Colors.indigo,
                      highlightColor: Colors.white,
                      textTheme: TextTheme(
                        headline5: TextStyle(
                            fontSize: 20.0, color: Colors.white, height: 1.3),
                        headline4: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            height: 1.3),
                        headline3: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.w300,
                            color: Colors.white,
                            height: 1.3),
                        headline2: TextStyle(
                            fontSize: 22.0,
                            fontWeight: FontWeight.w300,
                            color: Colors.white,
                            height: 1.4),
                        headline1: TextStyle(
                            fontSize: 24.0,
                            fontWeight: FontWeight.w300,
                            color: Colors.white,
                            height: 1.4),
                        subtitle1: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            height: 1.3),
                        headline6: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w300,
                            color: Colors.white,
                            height: 1.3),
                        bodyText2: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                            height: 1.2),
                        bodyText1: TextStyle(
                            fontSize: 13.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                            height: 1.3),
                        caption: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w300,
                            color: Colors.white,
                            height: 1.2),
                      ),
                    ));
        });
  }
}
