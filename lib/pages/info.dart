import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:sillytrack/repository/settings_repository.dart';

class InfoWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  InfoWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _InfoWidgetState createState() => _InfoWidgetState();
}

class _InfoWidgetState extends StateMVC<InfoWidget> {
  bool _isDark = setting.value.brightness.value == Brightness.dark;

  void changeMode() {
    setting.value.brightness.value =
        _isDark ? Brightness.dark : Brightness.light;
    setBrightness(_isDark ? Brightness.dark : Brightness.light);
    setting.notifyListeners();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Theme.of(context).accentColor,
            elevation: 0,
            centerTitle: true,
            title: Text(
              "Info",
              style: Theme.of(context).textTheme.headline5,
            ),
            actions: <Widget>[
              Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _isDark
                        ? Icon(
                            Icons.brightness_2,
                            color: Colors.white,
                          )
                        : Icon(Icons.brightness_4, color: Colors.white),
                    Transform.scale(
                        scale: 0.8,
                        child: CupertinoSwitch(
                          value: _isDark,
                          onChanged: (value) {
                            setState(() {
                              _isDark = !_isDark;
                              changeMode();
                            });
                          },
                        )),
                  ])
            ]),
        body: Column(children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Container(
                    padding: EdgeInsets.all(50),
                    child: Column(children: <Widget>[
                      Center(
                        child: Image.asset(
                          'assets/img/logo-alt.png',
                          width: 100,
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(height: 10),
                      Center(
                        child: Text(
                          "Sillytrack",
                          style: Theme.of(context)
                              .textTheme
                              .headline2
                              .merge(TextStyle(fontWeight: FontWeight.w500)),
                        ),
                      ),
                      SizedBox(height: 10),
                      Center(
                        child: Text(
                          "www.sillystack.com",
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ),
                      SizedBox(height: 10),
                      Center(
                        child: Text(
                          "Versi 1.0.0",
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ),
                    ])),
              ),
            ],
          )
        ]));
  }
}
