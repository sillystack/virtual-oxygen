import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:sillytrack/repository/settings_repository.dart';
import '../models/route_argument.dart';

class HomeWidget extends StatefulWidget {
  final GlobalKey<ScaffoldState> parentScaffoldKey;

  HomeWidget({Key key, this.parentScaffoldKey}) : super(key: key);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends StateMVC<HomeWidget> {
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool _isDark = setting.value.brightness.value == Brightness.dark;

  void changeMode() {
    setting.value.brightness.value =
        _isDark ? Brightness.dark : Brightness.light;
    setBrightness(_isDark ? Brightness.dark : Brightness.light);
    setting.notifyListeners();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Theme.of(context).accentColor,
            elevation: 0,
            centerTitle: true,
            title: Text(
              "Cek Resi",
              style: Theme.of(context).textTheme.headline5,
            ),
            actions: <Widget>[
              Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _isDark
                        ? Icon(
                            Icons.brightness_2,
                            color: Colors.white,
                          )
                        : Icon(Icons.brightness_4, color: Colors.white),
                    Transform.scale(
                        scale: 0.8,
                        child: CupertinoSwitch(
                          value: _isDark,
                          onChanged: (value) {
                            setState(() {
                              _isDark = !_isDark;
                              changeMode();
                            });
                          },
                        )),
                  ])
            ]),
        body: SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    color: Theme.of(context).scaffoldBackgroundColor,
                    margin: EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    padding:
                        const EdgeInsets.only(top: 30, left: 20, right: 20),
                    child: Text("asdasd"),
                  )
                ])));
  }
}
