import 'package:flutter/material.dart';
import 'package:sillytrack/pages/info.dart';

import '../helpers/helper.dart';
import '../models/route_argument.dart';
import '../pages/home.dart';

// ignore: must_be_immutable
class PagesWidget extends StatefulWidget {
  dynamic currentTab;
  RouteArgument routeArgument;
  Widget currentPage = HomeWidget();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  PagesWidget({
    Key key,
    this.currentTab,
  }) {
    if (currentTab != null) {
      if (currentTab is RouteArgument) {
        routeArgument = currentTab;
        currentTab = int.parse(currentTab.id);
      }
    } else {
      currentTab = 2;
    }
  }

  @override
  _PagesWidgetState createState() {
    return _PagesWidgetState();
  }
}

class _PagesWidgetState extends State<PagesWidget> {
  initState() {
    super.initState();
    _selectTab(widget.currentTab);
  }

  @override
  void didUpdateWidget(PagesWidget oldWidget) {
    _selectTab(oldWidget.currentTab);
    super.didUpdateWidget(oldWidget);
  }

  void _selectTab(int tabItem) {
    setState(() {
      widget.currentTab = tabItem;
      switch (tabItem) {
        case 0:
          widget.currentPage =
              HomeWidget(parentScaffoldKey: widget.scaffoldKey);
          break;
        case 1:
          widget.currentPage =
              InfoWidget(parentScaffoldKey: widget.scaffoldKey);
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: Helper.of(context).onWillPop,
      child: Scaffold(
          key: widget.scaffoldKey,
          // drawer: DrawerWidget(),
          // endDrawer: FilterWidget(onFilter: (filter) {
          //   Navigator.of(context)
          //       .pushReplacementNamed('/Pages', arguments: widget.currentTab);
          // }),
          body: widget.currentPage,
          bottomNavigationBar: Container(
            child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              selectedItemColor: Theme.of(context).focusColor,
              selectedFontSize: 0,
              unselectedFontSize: 0,
              iconSize: 22,
              elevation: 0,
              backgroundColor: Colors.transparent,
              selectedIconTheme: IconThemeData(size: 28),
              unselectedItemColor:
                  Theme.of(context).focusColor.withOpacity(0.5),
              currentIndex: widget.currentTab,
              onTap: (int i) {
                this._selectTab(i);
              },
              // this will be set when a new tab is tapped
              items: [
                BottomNavigationBarItem(
                  icon: Icon(Icons.receipt_long_rounded),
                  title: new Container(height: 0.0),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.info_outline),
                  title: new Container(height: 0.0),
                ),
              ],
            ),
          )),
    );
  }
}
